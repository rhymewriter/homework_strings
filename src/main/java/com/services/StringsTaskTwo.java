package main.java.com.services;

public class StringsTaskTwo {

    public String getStringFromInt (int num){
        String str = ""+ num;
//       String str = Integer.toString(num);
        return str;
    }

    public String getStringFromDouble (double num){
        String str = ""+ num;
//       String str = Double.toString(num);
        return str;
    }
    public int getIntFromString (String num){
        int number = Integer.parseInt(num);
//       int number = Integer.valueOf(num);
        return number;
    }

    public double getDoubleFromString (String num){
        double number = Double.parseDouble(num);
//       double number = Double.valueOf(num);
        return number;
    }
}
