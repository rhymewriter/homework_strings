package main.java.com.services;

import java.util.Arrays;

public class StringsTask {

    public String getCharsFromAToZ() {
        char[] chars = new char[26];
        int count = 0;
        for (char i = 'a'; i <= 'z'; i++) {
            chars[count] = i;
            count++;
        }
        return String.valueOf(chars).toUpperCase();
    }

    public String getCharsFromZToA() {
        char[] chars = new char[26];
        int count = 0;
        for (char i = 'z'; i >= 'a'; i--) {
            chars[count] = i;
            count++;
        }
        return String.valueOf(chars).toUpperCase();
    }

    public String getCharsFromRusAToRusYa() {
        char[] chars = new char[33];
        int count = 0;
        for (char i = 'а'; i >= 'я'; i++) {
            chars[count] = i;
            count++;
        }
        return String.valueOf(chars).toUpperCase();
    }

    public String getCharsFromRusYaToRusA() {
        char[] chars = new char[33];
        int count = 0;
        for (char i = 'я'; i <= 'а'; i--) {
            chars[count] = i;
            count++;
        }
        return String.valueOf(chars).toUpperCase();
    }

    public  String getArabicNumerals() {
        String numbers = "";
        for (int i = 0; i < 10; i++) {
            numbers += i;
        } return numbers;
    }
    public String getCharsPrintableAscii() {
        char[] chars = new char[94];
        int count = 0;
        for (char i = 33; i <= 126; i--) {
            chars[count] = i;
            count++;
        }
        return String.valueOf(chars);
    }



}
