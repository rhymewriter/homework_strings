package main.java.com.services;

public class StringsTaskThree {

    public int getLengthOfSmollestWord(String str) {
        String[] strArr = str.replaceAll("[^a-zA-Z ]", "").toLowerCase().split("\\s+");
        int minSize = strArr[0].length();
        for (int i = 1; i < strArr.length; i++) {
            if (strArr[i].length() < minSize) {
                minSize = strArr[i].length();
            }
        }
        return minSize;
    }

    public String[] replaceThreeLastCharsToS(String[] words, int lengthWord){
        if(lengthWord<3)
        {
            return null;
        }
        for (int i=0; i<words.length; i++)
        {
            words[i].trim();
            if(words[i].length()==lengthWord){
                words[i] = words[i].substring(0,(words[i].length()-3));
                words[i] = words[i].concat("$$$");
            }
        }

        return words;
    }


    public String addSpace(String str)
    {
        final String[] punctSymbols = new String[]{".", ",", ":", "?", "!"};
        for (int j = 0; j < punctSymbols.length; j++)
        {
            String addSpace = punctSymbols[j]+" ";
            str=str.replace(punctSymbols[j],  addSpace);
            str=str.replace("  ",  " ");
        }
        return str;
    }


    public String leaveOnlyUniqueSimbol(String str) {
        String result = "";
        for (int i=0; i<str.length();i++){
            String repeatChar = ""+str.charAt(i);
            if(!result.contains(repeatChar))
            {
                result = result.concat(repeatChar);
            }
        }
        return result;
    }

    public int numbersOfString(String str){
        String[] words = str.split("[\\s.,?!:-]+");
        return words.length;
    }

    public String cutPartFromPosition(String str, int startCut, int endCut){
        String firstPart = str.substring(0, startCut);
        String secondPart = str.substring(endCut);
        str = firstPart + secondPart;
        return str;
    }

    public String reversWord(String str)
    {
        String reversWord = "";
        for (int i=str.length()-1; i>=0;i--){
            char symbol = str.charAt(i);
            reversWord = reversWord+symbol;
        }
        return reversWord;
    }

    public String delLastWord(String str)
    {

        String[] line = str.split("[\\s.,?!:-]+");
        String lastWord = line[line.length-1];
        String result = str.replace(lastWord, "");
        return result;
    }

}
