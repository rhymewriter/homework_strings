package main.java.com;

import main.java.com.services.StringsTaskThree;
import main.java.com.services.StringsTaskTwo;
import main.java.com.services.StringsTask;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        System.out.println("Раздел №1");
        StringsTask stringsTask = new StringsTask();
        String printCharsAZ = stringsTask.getCharsFromAToZ();
        System.out.println(printCharsAZ);
        String printCharsZA = stringsTask.getCharsFromZToA();
        System.out.println(printCharsZA);
        String printCharsRusAToRusYa = stringsTask.getCharsFromRusAToRusYa();
        System.out.println(printCharsRusAToRusYa);
        String printCharsRusYaToRusA = stringsTask.getCharsFromRusYaToRusA();
        System.out.println(printCharsRusYaToRusA);
        String getArabicNum = stringsTask.getArabicNumerals();
        System.out.println(getArabicNum);
        String getCharsAscii = stringsTask.getCharsPrintableAscii();
        System.out.println(getCharsAscii);

        System.out.println("Раздел №2");
        StringsTaskTwo stringsTaskTwo = new StringsTaskTwo();
        String fromIntToString = stringsTaskTwo.getStringFromInt(10545);
        System.out.println(fromIntToString);
        String fromDoubleToString = stringsTaskTwo.getStringFromDouble(32.456);
        System.out.println(fromDoubleToString);
        int fromStringToInt = stringsTaskTwo.getIntFromString("2796");
        System.out.println(fromStringToInt);
        double fromStringToDouble = stringsTaskTwo.getDoubleFromString("45.36");
        System.out.println(fromStringToDouble);

        System.out.println("Раздел №3");
        StringsTaskThree stringsTaskThree = new StringsTaskThree();
        System.out.println(stringsTaskThree.getLengthOfSmollestWord("Legends!! Trolls..., " +
                "contribution? deus;-: firefly, victory"));
        System.out.println(Arrays.toString(stringsTaskThree.replaceThreeLastCharsToS(new String[]
                {"слова", "молва", "конва"}, 5)));
        System.out.println(stringsTaskThree.addSpace("слова,конфеты!!сладкие:до?ушей"));
        System.out.println(stringsTaskThree.leaveOnlyUniqueSimbol("слова,конфеты!!сладкие:до?ушей"));
        System.out.println(stringsTaskThree.numbersOfString("слова - конфеты сладкие до ушей"));
        System.out.println(stringsTaskThree.cutPartFromPosition("европеоидный", 2, 8));
        System.out.println(stringsTaskThree.reversWord("аргентинаманитнегра"));
        System.out.println(stringsTaskThree.delLastWord("аргентина манит негра"));
    }
}
